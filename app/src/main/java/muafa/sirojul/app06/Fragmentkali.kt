package muafa.sirojul.app06

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragkali.*
import kotlinx.android.synthetic.main.fragkali.view.*
import java.text.DecimalFormat

class Fragmentkali : Fragment() , View.OnClickListener{
    lateinit var thisParent: MainActivity
    lateinit var v : View
    var a : Double = 0.0
    var b : Double = 0.0
    var hasil : Double = 0.0

    override fun onClick(v: View?) {
        a = a1.text.toString().toDouble()
        b = b2.text.toString().toDouble()
        hasil = a*b
        hasilkali.text = DecimalFormat("#.##").format(hasil)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v =  inflater.inflate(R.layout.fragkali,container,false)
        v.Perkali.setOnClickListener(this)
        return v
    }
}