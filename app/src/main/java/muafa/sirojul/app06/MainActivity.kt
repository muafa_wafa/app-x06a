package muafa.sirojul.app06

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var fragProdi: FragmentProdi
    lateinit var fragMhs: FragmentMahasiswa
    lateinit var ft: FragmentTransaction
    lateinit var fragkali: Fragmentkali

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragkali = Fragmentkali()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.itemProdi -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragProdi).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 225))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragMhs).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 225, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> frameLayout.visibility = View.GONE

            R.id.itemkali ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragkali).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 225, 255, 255))
                frameLayout.visibility = View.VISIBLE

                true
            }

            R.id.itemsitus ->{
            var weburi = "https://polinema.ac.id"
            var intentinternet = Intent(Intent.ACTION_VIEW, Uri.parse(weburi))
            startActivity(intentinternet)
        }

        }
        return true
    }
}


